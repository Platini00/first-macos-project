//
//  AppDelegate.swift
//  First-macos-project
//
//  Created by Phil Looby on 19/11/2020.
//  Copyright © 2020 Phil Looby. All rights reserved.
//

import Cocoa

@NSApplicationMain
class AppDelegate: NSObject, NSApplicationDelegate {



    func applicationDidFinishLaunching(_ aNotification: Notification) {
        // Insert code here to initialize your application
    }

    func applicationWillTerminate(_ aNotification: Notification) {
        // Insert code here to tear down your application
    }


}

